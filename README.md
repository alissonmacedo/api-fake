# API FAKE

Este repositório contém uma simples API para o desafio de front-end

## Como executar

Clone este repositório, execute `npm install` ou `yarn` para instalar as dependencias

Depois rode `yarn json-server db.json`.

A API fica localizada em `http://localhost:3000`.

## Rotas

Todas as requisições de POST para esta API devem conter o header `Content-Type: application/json`.
Esta API contém as seguintes rotas:

- `GET /restaurant` : lista as ferramentas cadastradas
- `POST /restaurant` : cria uma nova ferramenta
- `DELETE /restaurant/:id` : apaga a ferramenta com ID :id

Para filtrar as ferramentas em `GET /restaurant`, é possível:

- fazer uma busca global utilizando a query string `?q=:busca`;
- fazer uma busca por tags individuais utilizando a query string `?tags_like=:busca`.

## Exemplos

### GET /restaurant

Requisição:

```javascript
GET / restaurant;
```

Resposta:

```javascript
[
  {
    id: 1,
    title: "Notion",
    link: "https://notion.so",
    description:
      "All in one tool to organize teams and ideas. Write, plan, collaborate, and get organized. ",
    tags: ["organization", "planning", "collaboration", "writing", "calendar"],
  },
  {
    id: 2,
    title: "json-server",
    link: "https://github.com/typicode/json-server",
    description:
      "Fake REST API based on a json schema. Useful for mocking and creating APIs for front-end devs to consume in coding challenges.",
    tags: ["api", "json", "schema", "node", "github", "rest"],
  },
  {
    id: 3,
    title: "fastify",
    link: "https://www.fastify.io/",
    description:
      "Extremely fast and simple, low-overhead web framework for NodeJS. Supports HTTP2.",
    tags: ["web", "framework", "node", "http2", "https", "localhost"],
  },
];
```

### GET /restaurant?q=:busca

Requisição:

```javascript
GET /restaurant?q=Amburgueria
```

Resposta:

```javascript
[
  {
    id: 3,
    name: "Amburgueria SalzBurg",
    link: "www.salzburg.com",
    description: "Um amburguer incrível.",
    tag: ["Comida", "Amburguer", "Gourmet"],
  },
];
```

### GET /restaurant?tags_like=:busca

Requisição:

```javascript
GET /restaurant?tags_like=comida
```

Resposta:

```javascript
[
  {
    id: 1,
    name: "Pizzaria Napole",
    link: "www.pizzarianapole.com",
    description: "A melhor pizza de toda a resgião.",
    tag: ["Comida", "Pizza"],
  },
  {
    id: 2,
    name: "Lanchonee Nota10",
    link: "www.lanchonetenota10.com",
    description: "O melhor lanche que você ja comeu.",
    tag: ["Comida", "Lanche"],
  },
  {
    id: 3,
    name: "Amburgueria SalzBurg",
    link: "www.salzburg.com",
    description: "Um amburguer incrível.",
    tag: ["Comida", "Amburguer", "Gourmet"],
  },
];
```

### POST /restaurants

Requisição:

```javascript
// POST /tools
// Content-Type: application/json
{
    "id": 6,
    "name": "Teste",
    "link": "www.teste.com",
    "description": "teste.",
    "tag": [
        "Comida",
        "Teste"
    ]
}
```

Resposta:

```javascript
{
    "id": 6,
    "name": "Teste",
    "link": "www.teste.com",
    "description": "teste.",
    "tag": [
        "Comida",
        "Teste"
    ]
}
```

### DELETE /restaurants/:id

Requisição:

```javascript
DELETE / restaurants / 3;
```

Resposta:

```javascript
// Status: 200 OK
{
}
```
